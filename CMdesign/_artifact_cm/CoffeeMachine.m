# makefile for System: CoffeeMachine

sctAUTOCFGDEP =
sctASN1AUTOCFGDEP =
sctCOMPFLAGS = -DXUSE_GENERIC_FUNC

include $(sctdir)/makeoptions

default: CoffeeMachine$(sctEXTENSION)

CoffeeMachine$(sctEXTENSION): \
  CoffeeMachine$(sctOEXTENSION) \
  U2ctypes$(sctOEXTENSION) \
  U2ExtraOps$(sctOEXTENSION) \
  CMdesign$(sctOEXTENSION) \
  $(sctLINKKERNELDEP) \
  $(sctLINKCODERLIBDEP)
	$(sctLD)  CoffeeMachine$(sctOEXTENSION) U2ctypes$(sctOEXTENSION) U2ExtraOps$(sctOEXTENSION) CMdesign$(sctOEXTENSION) $(sctLINKKERNEL) $(sctLINKCODERLIB)  $(sctLDFLAGS) -o CoffeeMachine$(sctEXTENSION)

CoffeeMachine$(sctOEXTENSION): \
  CoffeeMachine.c \
  U2ctypes.h \
  U2ExtraOps.h \
  U2ctypes.h \
  U2ExtraOps.h \
  CMdesign.h \
  CMdesign.h \
  U2ctypes.h \
  U2ExtraOps.h
	$(sctCC) $(sctCPPFLAGS) $(sctCCFLAGS) $(sctIFDEF) CoffeeMachine.c -o CoffeeMachine$(sctOEXTENSION)

U2ctypes$(sctOEXTENSION): \
  U2ctypes.c \
  U2ctypes.h
	$(sctCC) $(sctCPPFLAGS) $(sctCCFLAGS) $(sctIFDEF) U2ctypes.c -o U2ctypes$(sctOEXTENSION)

U2ExtraOps$(sctOEXTENSION): \
  U2ExtraOps.c \
  U2ExtraOps.h
	$(sctCC) $(sctCPPFLAGS) $(sctCCFLAGS) $(sctIFDEF) U2ExtraOps.c -o U2ExtraOps$(sctOEXTENSION)

CMdesign$(sctOEXTENSION): \
  CMdesign.c \
  CMdesign.h
	$(sctCC) $(sctCPPFLAGS) $(sctCCFLAGS) $(sctIFDEF) CMdesign.c -o CMdesign$(sctOEXTENSION)

clean: clean_gen_objs clean_kernel_objs

clean_gen_objs:
	$(sctRM) \
  CoffeeMachine$(sctOEXTENSION) \
  U2ctypes$(sctOEXTENSION) \
  U2ExtraOps$(sctOEXTENSION) \
  CMdesign$(sctOEXTENSION) \
  CoffeeMachine$(sctEXTENSION)
